## How to use Container
Just get the Compose Project from and compose it
```console 
 git clone https://gitlab.com/web589/compose/-/tree/main
 cd compose
 docker-compose up
```



## Configuration Parameters
The Parameters are defined in the following file: https://gitlab.com/web589/compose/-/blob/main/compose.yaml \
We are using:
| Name | Description |
| ----------- | ----------- |
BACKEND_RESERVATIONS_NAME| Name to access the Reservations
BACKEND_RESERVATIONS_PORT| Port for the Reservations
JAEGER_TRACECONTEXTHEADERNAME| Name for the Jaeger Tracing
KEYCLOAK_HOST| Authentication Host
KEYCLOAK_REALM| Realm for the Authentication
POSTGRES_ASSETS_USER| Username for the Database User
POSTGRES_ASSETS_PASSWORD| Password for the Postgres Database
POSTGRES_ASSETS_DBNAME| Name of the Postgres Database
POSTGRES_ASSETS_HOST| Database Host
POSTGRES_ASSETS_PORT| Database Port

