package database

import (
	"os"

	"github.com/go-pg/pg/v10"
)

func Connect() *pg.DB {
	var db = pg.Connect(&pg.Options{
		User:     os.Getenv("POSTGRES_ASSETS_USER"),
		Password: os.Getenv("POSTGRES_ASSETS_PASSWORD"),
		Database: os.Getenv("POSTGRES_ASSETS_DBNAME"),
		Addr:     os.Getenv("POSTGRES_ASSETS_HOST") + ":" + os.Getenv("POSTGRES_ASSETS_PORT"),
	})
	if db == nil {
		os.Stderr.WriteString("Database connection failed")
		os.Exit(100)
	}
	os.Stdout.WriteString("Connected to db")

	return db
}
