package assets

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg/v10"
	"github.com/google/uuid"
)

type Room struct {
	Id       uuid.UUID `pg:",pk,notnull,type:uuid, default:uuid_generate_v4()"json:"id"`
	Name     string    `pg:",notnull" json:"name"`
	StoreyId uuid.UUID `pg:",notnull, type:uuid" json:"storey_id"`
	Storey   *Storey   `pg:",rel:has-one" json:"storey,omitempty"`
}

type RoomStrings struct {
	Id       string `json:"id"`
	StoreyId string `json:"storey_id"`
	Name     string `json:"name"`
}

type Reservations struct {
	Id      string `json:"id"`
	From    string `json:"from"`
	To      string `json:"to"`
	Room_id string `json:"room_id"`
}

func GetRooms(c *gin.Context) {
	db := c.MustGet("databaseConn").(*pg.DB)
	var rooms []Room
	if storey_id, ok := c.GetQuery("storey_id"); ok || storey_id != "" {
		//storey_id as Parameter
		err := db.Model(&rooms).Where("storey_id =?", storey_id).Select()
		if err != nil {
			os.Stderr.WriteString(err.Error())
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"Message": err.Error()})
			return
		}
		if rooms == nil {
			os.Stderr.WriteString("Storey not found")
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"Message": "Storey not found"})
			return
		}
		c.IndentedJSON(http.StatusOK, rooms)
		return
	}
	err := db.Model(&rooms).Select()
	if err != nil {
		os.Stderr.WriteString(err.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"Message": err.Error()})
		return
	}
	c.IndentedJSON(http.StatusOK, rooms)
	return
}

func GetRoomsById(c *gin.Context) {
	db := c.MustGet("databaseConn").(*pg.DB)
	room := new(Room)
	roomId := c.Param("id")
	err := db.Model(room).Where("room.id = ?", roomId).Select()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": "Room not found"})
		os.Stderr.WriteString(err.Error())
		return
	}
	c.IndentedJSON(http.StatusOK, room)
}

func AddRoom(c *gin.Context) {
	db := c.MustGet("databaseConn").(*pg.DB)
	room := new(Room)
	roomStrings := new(RoomStrings)
	if err := c.BindJSON(&roomStrings); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		os.Stderr.WriteString(err.Error())
		return
	}

	if roomStrings.Name == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "No Name was given"})
		os.Stderr.WriteString("\nempty room name\n")
		return
	}

	room.Name = roomStrings.Name
	room.StoreyId = uuid.MustParse(roomStrings.StoreyId)
	countStorey, _ := db.Model(new(Storey)).Where("id = ?", room.StoreyId).Count()
	if countStorey == 0 {
		c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{"message": "Storey not found"})
		return
	}
	if roomStrings.Id != "" {
		room.Id = uuid.MustParse(roomStrings.Id)

		if _, err := db.Model(room).WherePK().Update(); err != nil {
			//error case
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Database error"})
			os.Stderr.WriteString(err.Error())
			return
		} else {
			//send updated over api
			UpdatedRoom := db.Model(room).WherePK().Select()
			c.IndentedJSON(http.StatusOK, UpdatedRoom)
			return
		}
	}
	_, err := db.Model(room).Returning("id").Insert()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Internal Server error"})
		os.Stderr.WriteString(err.Error())
		return
	}
	c.IndentedJSON(http.StatusCreated, room)
}

func AddRoomById(c *gin.Context) {
	db := c.MustGet("databaseConn").(*pg.DB)
	room := new(Room)
	roomStrings := new(RoomStrings)
	if err := c.BindJSON(&roomStrings); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		os.Stderr.WriteString(err.Error())
		return
	}

	if roomStrings.Name == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "No Name was given"})
		return
	}

	room.Name = roomStrings.Name
	room.StoreyId = uuid.MustParse(roomStrings.StoreyId)
	countStorey, _ := db.Model(new(Storey)).Where("id = ?", room.StoreyId).Count()
	if countStorey == 0 {
		c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{"message": "Storey not found"})
		os.Stderr.WriteString("Storey not found")
		return
	}

	if roomStrings.Id != c.Param("id") {
		c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{"message": "ID in body doesnt match ID in URL"})
		os.Stderr.WriteString("ID in body doesnt match ID in URL")
		return
	}
	if roomStrings.Id != "" {
		room.Id = uuid.MustParse(roomStrings.Id)

		count, dberror := db.Model(room).WherePK().Count()
		if count >= 1 && dberror == nil {
			if _, err := db.Model(room).WherePK().Update(); err != nil {
				//error case
				c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Database error"})
				os.Stderr.WriteString(err.Error())
				return
			} else {
				//send updated over api
				c.Status(http.StatusNoContent)
				return
			}
		} else if count == 0 && dberror == nil {
			_, err := db.Model(room).Insert()
			if err != nil {
				c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Internal Server error"})
				os.Stderr.WriteString(err.Error())
				return
			}
			c.Status(http.StatusNoContent)
			return
		} else {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Database error"})
			os.Stdout.WriteString(dberror.Error())
			return
		}

	}
}

func DeleteRoomById(c *gin.Context) {
	db := c.MustGet("databaseConn").(*pg.DB)
	id := uuid.MustParse(c.Param("id"))

	reservation := []Reservations{}
	req, err := http.NewRequest("GET", "http://"+os.Getenv("BACKEND_RESERVATIONS_NAME")+os.Getenv("BACKEND_RESERVATIONS_PORT")+"/reservations/", nil)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		os.Stderr.WriteString(err.Error())
		return
	}
	var jaegerTracecontextheadername = "uber-trace-id"
	val, ok := os.LookupEnv("JAEGER_TRACECONTEXTHEADERNAME")
	if ok {
		jaegerTracecontextheadername = val
	}
	if c.Request.Header[jaegerTracecontextheadername] != nil {
		req.Header.Set(jaegerTracecontextheadername, c.Request.Header.Get(jaegerTracecontextheadername))
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		os.Stderr.WriteString(err.Error())
	} else {
		fmt.Println(resp.Body)
		defer resp.Body.Close()
		responseBytes, _ := ioutil.ReadAll(resp.Body)
		json.Unmarshal(responseBytes, &reservation)
		jsonString := string(responseBytes)
		fmt.Println(jsonString)
		fmt.Println(reservation)

		for _, element := range reservation {
			if element.Room_id == c.Param("id") {
				c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{"message": "existing reservation for room"})
				os.Stdout.WriteString("existing reservation for room")
				return
			}

		}

	}

	room := new(Room)
	res, _ := db.Model(room).Where("id = ?", id).Delete()
	if res.RowsAffected() == 0 {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": "Room not Found"})
		os.Stdout.WriteString("Room not Found")
		return
	}
	c.Status(http.StatusNoContent)
}
