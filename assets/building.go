package assets

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg/v10"
	"github.com/google/uuid"
)

type Building struct {
	Id      uuid.UUID `pg:",pk,notnull,type:uuid, default:uuid_generate_v4()"json:"id"`
	Name    string    `pg:",notnull" json:"name"`
	Address string    `json:"address"`
}
type BuildingStrings struct {
	UUID    string `json:"id"`
	Name    string `json:"name"`
	Address string `json:"address"`
}

func GetBuildings(c *gin.Context) {
	db := c.MustGet("databaseConn").(*pg.DB)
	var buildings []Building
	err := db.Model(&buildings).Select()
	if err != nil {
		os.Stderr.WriteString(err.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"Message": err.Error()})
		return
	}
	c.IndentedJSON(http.StatusOK, buildings)
}

func GetBuildingById(c *gin.Context) {
	db := c.MustGet("databaseConn").(*pg.DB)
	building := new(Building)
	buildingId := c.Param("id")
	err := db.Model(building).Where("id = ?", buildingId).Select()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": "Building not found"})
		os.Stderr.WriteString(err.Error())
		return
	}
	c.IndentedJSON(http.StatusOK, building)
}

type ParseErrorCode int

const (
	ParsedOk ParseErrorCode = 0
	NoName                  = 1
	NoUUID                  = 2
)

func buildingsStringToBuilding(buildingStrings *BuildingStrings, building *Building) ParseErrorCode {
	if buildingStrings.Name == "" {
		return NoName
	}
	building.Name = buildingStrings.Name
	building.Address = buildingStrings.Address

	if buildingStrings.UUID == "" {
		return NoUUID
	} else {
		building.Id = uuid.MustParse(buildingStrings.UUID)
	}
	return ParsedOk
}

func AddBuilding(c *gin.Context) {
	db := c.MustGet("databaseConn").(*pg.DB)
	building := new(Building)
	buildingStrings := new(BuildingStrings)
	if err := c.BindJSON(&buildingStrings); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		os.Stderr.WriteString(err.Error())
		return
	}

	parseResult := buildingsStringToBuilding(buildingStrings, building)
	if parseResult == NoName {

		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "No name was given."})
		return
	}

	if parseResult == ParsedOk {

		if _, err := db.Model(building).WherePK().Update(); err != nil {
			//error case
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Database error"})
			os.Stderr.WriteString(err.Error())
			return
		} else {
			//send updated over api
			UpdatedBuilding := db.Model(building).WherePK().Select()
			c.IndentedJSON(http.StatusOK, UpdatedBuilding)
			return
		}
	}
	_, err := db.Model(building).Returning("id").Insert()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Internal Server error"})
		os.Stderr.WriteString(err.Error())
		return
	}
	c.IndentedJSON(http.StatusCreated, building)
}

func AddBuildingById(c *gin.Context) {
	db := c.MustGet("databaseConn").(*pg.DB)
	building := new(Building)
	buildingStrings := new(BuildingStrings)
	if err := c.BindJSON(&buildingStrings); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		os.Stderr.WriteString(err.Error())
		return
	}

	if buildingStrings.Name == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "No Name was given"})
		return
	}

	building.Name = buildingStrings.Name
	building.Address = buildingStrings.Address
	if buildingStrings.UUID != c.Param("id") {
		c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{"message": "ID in body doesnt match ID in URL"})
		return
	}
	if buildingStrings.UUID != "" {
		building.Id = uuid.MustParse(buildingStrings.UUID)

		count, dberror := db.Model(building).WherePK().Count()
		if count >= 1 && dberror == nil {
			if _, err := db.Model(building).WherePK().Update(); err != nil {
				//error case
				c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Database error"})
				os.Stderr.WriteString(err.Error())
				return
			} else {
				//send updated over api
				c.Status(http.StatusNoContent)
				return
			}
		} else if count == 0 && dberror == nil {
			_, err := db.Model(building).Insert()
			if err != nil {
				c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Internal Server error"})
				os.Stderr.WriteString(err.Error())
				return
			}
			c.Status(http.StatusNoContent)
			return
		} else {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Database error"})
			os.Stderr.WriteString(dberror.Error())
			return
		}

	}
}

func DeleteBuildingById(c *gin.Context) {
	db := c.MustGet("databaseConn").(*pg.DB)
	id := uuid.MustParse(c.Param("id"))
	building := new(Building)

	storey := new(Storey)
	count, err := db.Model(storey).Where("building_Id = ?", id).Count()
	if count > 0 {
		c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{"message": "deletion not possible because of existing storeys"})
		return
	}
	if err != nil {
		os.Stderr.WriteString(err.Error())
	}

	res, err := db.Model(building).Where("id = ?", id).Delete()
	if res.RowsAffected() == 0 {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": "Building not Found"})
		return
	}
	if err != nil {
		os.Stderr.WriteString(err.Error())
	}
	c.Status(http.StatusNoContent)
}
