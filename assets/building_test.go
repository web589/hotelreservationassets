package assets

import (
	"testing"
)

func TestBuildingsStringToBuildingNoNameGiven(t *testing.T) {
	building := new(Building)
	buildingStrings := new(BuildingStrings)
	buildingStrings.UUID = "2dfef7e2-0c91-4b9d-a730-03ac32266292"
	buildingStrings.Name = ""
	buildingStrings.Address = "ŝablono strato 42"

	var expected ParseErrorCode = NoName
	result := buildingsStringToBuilding(buildingStrings, building)

	if result != expected {
		t.Fatalf(`BuildingsStringToBuilding expected = %v received = %v`, result, expected)
	}

}

func TestBuildingsStringToBuildingNoUUIDGiven(t *testing.T) {
	building := new(Building)
	buildingStrings := new(BuildingStrings)
	buildingStrings.UUID = ""
	buildingStrings.Name = "DHBW"
	buildingStrings.Address = "ŝablono strato 42"

	var expected ParseErrorCode = NoUUID
	result := buildingsStringToBuilding(buildingStrings, building)

	if result != expected {
		t.Fatalf(`BuildingsStringToBuilding expected = %v received = %v`, result, expected)
	}

}

func TestBuildingsStringToBuildingValidParameters(t *testing.T) {
	building := new(Building)
	buildingStrings := new(BuildingStrings)
	buildingStrings.UUID = "2dfef7e2-0c91-4b9d-a730-03ac32266292"
	buildingStrings.Name = "DHBW"
	buildingStrings.Address = "ŝablono strato 42"

	var expected ParseErrorCode = ParsedOk
	result := buildingsStringToBuilding(buildingStrings, building)

	if result != expected {
		t.Fatalf(`BuildingsStringToBuilding expected = %v received = %v`, result, expected)
	}

}
