package assets

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg/v10"
	"github.com/google/uuid"
)

type Storey struct {
	Id         uuid.UUID `pg:",pk,notnull,type:uuid, default:uuid_generate_v4()"json:"id"`
	Name       string    `pg:",notnull" json:"name"`
	BuildingId uuid.UUID `pg:",notnull, type:uuid" json:"building_id"`
	Building   *Building `pg:",rel:has-one" json:"building,omitempty"`
}

type StoreyString struct {
	Id         string `json:"id"`
	Name       string `json:"name"`
	BuildingId string `json:"building_id"`
}

func GetStoreys(c *gin.Context) {

	db := c.MustGet("databaseConn").(*pg.DB)
	var storeys []Storey
	if building_id, ok := c.GetQuery("building_id"); ok || building_id != "" {
		//building_id as Parameter
		err := db.Model(&storeys).Where("building_id =?", building_id).Select()
		if err != nil {
			os.Stderr.WriteString(err.Error())
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"Message": err.Error()})
			return
		}
		if storeys == nil {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"Message": "Building not found"})
			return
		}
		c.IndentedJSON(http.StatusOK, storeys)
		return
	}

	err := db.Model(&storeys).Select()
	if err != nil {
		os.Stderr.WriteString(err.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"Message": err.Error()})
		return
	}
	c.IndentedJSON(http.StatusOK, storeys)
}

func GetStoreyById(c *gin.Context) {
	storeyId := c.Param("id")
	getStoreyById(c, storeyId)
}

func getStoreyById(c *gin.Context, id string) {
	db := c.MustGet("databaseConn").(*pg.DB)
	storey := new(Storey)
	if err := db.Model(storey).Where("storey.id = ?", id).Select(); err == nil {
		c.IndentedJSON(http.StatusOK, storey)
	} else {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": "Building not found"})
		os.Stderr.WriteString(err.Error())
	}
}

func AddStorey(c *gin.Context) {
	db := c.MustGet("databaseConn").(*pg.DB)
	storey := new(Storey)
	storeyStrings := new(StoreyString)
	if err := c.BindJSON(&storeyStrings); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		os.Stderr.WriteString(err.Error())
		return
	}

	if storeyStrings.Name == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "No Name was given"})
		return
	}

	storey.Name = storeyStrings.Name
	storey.BuildingId = uuid.MustParse(storeyStrings.BuildingId)
	countBuilding, _ := db.Model(new(Building)).Where("id = ?", storey.BuildingId).Count()
	if countBuilding == 0 {
		c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{"message": "Building not found"})
		return
	}
	if storeyStrings.Id != "" {
		storey.Id = uuid.MustParse(storeyStrings.Id)

		if _, err := db.Model(storey).WherePK().Update(); err != nil {
			//error case
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Database error"})
			os.Stderr.WriteString(err.Error())
			return
		} else {
			//send updated over api
			UpdatedStorey := db.Model(storey).WherePK().Select()
			c.IndentedJSON(http.StatusOK, UpdatedStorey)
			return
		}
	}
	_, err := db.Model(storey).Returning("id").Insert()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Internal Server error"})
		os.Stderr.WriteString(err.Error())
		return
	}
	c.IndentedJSON(http.StatusCreated, storey)
}

func AddStoreyById(c *gin.Context) {
	db := c.MustGet("databaseConn").(*pg.DB)
	storey := new(Storey)
	storeyStrings := new(StoreyString)
	if err := c.BindJSON(&storeyStrings); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		os.Stderr.WriteString(err.Error())
		return
	}

	if storeyStrings.Name == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "No Name was given"})
		return
	}

	storey.Name = storeyStrings.Name
	storey.BuildingId = uuid.MustParse(storeyStrings.BuildingId)
	countBuilding, _ := db.Model(new(Building)).Where("id = ?", storey.BuildingId).Count()
	if countBuilding == 0 {
		c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{"message": "Storey not found"})
		return
	}

	if storeyStrings.Id != c.Param("id") {
		c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{"message": "ID in body doesnt match ID in URL"})
		return
	}
	if storeyStrings.Id != "" {
		storey.Id = uuid.MustParse(storeyStrings.Id)

		count, error := db.Model(storey).WherePK().Count()
		if count >= 1 && error == nil {
			if _, err := db.Model(storey).WherePK().Update(); err != nil {
				//error case
				c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Database error"})
				os.Stderr.WriteString(err.Error())
				return
			} else {
				//send updated over api
				c.Status(http.StatusNoContent)
				return
			}
		} else if count == 0 && error == nil {
			_, err := db.Model(storey).Insert()
			if err != nil {
				c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Internal Server error"})
				os.Stderr.WriteString(err.Error())
				return
			}
			c.Status(http.StatusNoContent)
			return
		} else {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Database error"})
			return
		}

	}
}

func DeleteStoreyById(c *gin.Context) {
	db := c.MustGet("databaseConn").(*pg.DB)
	id := uuid.MustParse(c.Param("id"))
	storey := new(Storey)

	room := new(Room)
	count, err := db.Model(room).Where("storey_Id = ?", id).Count()
	if count > 0 {
		c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{"message": "deletion not possible because of existing rooms"})
		return
	}
	if err != nil {
		os.Stderr.WriteString(err.Error())
	}

	res, err := db.Model(storey).Where("id = ?", id).Delete()
	if res.RowsAffected() == 0 {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": "Building not Found"})
		return
	}
	if err != nil {
		os.Stderr.WriteString(err.Error())
	}
	c.Status(http.StatusNoContent)
}
