package main

import (
	"biletado/assets/assets"
	"biletado/assets/auth"

	"biletado/assets/database"

	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg/v10"
)

func ApiMiddleware(db *pg.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("databaseConn", db)
		c.Next()
	}
}

func TracingMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

	}
}

func main() {
	var db = database.Connect()
	router := gin.Default()
	router.Use(ApiMiddleware(db))

	router.GET("/assets/buildings/", assets.GetBuildings)
	router.GET("/assets/buildings/:id/", assets.GetBuildingById)
	router.POST("/assets/buildings/buildings/", auth.AuthorizationdMiddleware(), assets.AddBuilding)
	router.PUT("/assets/buildings/:id/", auth.AuthorizationdMiddleware(), assets.AddBuildingById)
	router.DELETE("/assets/buildings/:id/", auth.AuthorizationdMiddleware(), assets.DeleteBuildingById)

	router.GET("/assets/rooms/", assets.GetRooms)
	router.GET("/assets/rooms/:id/", assets.GetRoomsById)
	router.POST("/assets/rooms/", auth.AuthorizationdMiddleware(), assets.AddRoom)
	router.PUT("/assets/rooms/:id/", auth.AuthorizationdMiddleware(), assets.AddRoomById)
	router.DELETE("/assets/rooms/:id/", auth.AuthorizationdMiddleware(), assets.DeleteRoomById)

	router.GET("/assets/storeys/", assets.GetStoreys)
	router.GET("/assets/storeys/:id/", assets.GetStoreyById)
	router.POST("/assets/storeys/", auth.AuthorizationdMiddleware(), assets.AddStorey)
	router.PUT("/assets/storeys/:id/", auth.AuthorizationdMiddleware(), assets.AddStoreyById)
	router.DELETE("/assets/storeys/:id/", auth.AuthorizationdMiddleware(), assets.DeleteStoreyById)

	router.Any("/assets/jwt/", auth.AuthorizationdMiddleware(), auth.IsAuthorized)
	router.Run(":9000")
}
