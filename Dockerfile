FROM golang:1.17-alpine as builder

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

ENV CGO_ENABLED=0

COPY ./ ./

RUN go build -o /biletado /app

FROM scratch
COPY --from=builder /biletado /biletado
# the tls certificates:
COPY --from=alpine:latest /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENTRYPOINT ["/biletado"]

EXPOSE 9000
