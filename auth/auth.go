package auth

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"

	"github.com/gin-gonic/gin"
	jwt "github.com/golang-jwt/jwt"
)

type Realm struct {
	PublicKey string `json:"public_key"`
}

func IsAuthorized(c *gin.Context) {
	c.Status(http.StatusOK)
}

func AuthorizationdMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader, authHeaderOk := c.Request.Header["Authorization"]
		if !authHeaderOk {
			fmt.Printf("not authorized")
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		var host = os.Getenv("KEYCLOAK_HOST")
		var realmName = os.Getenv("KEYCLOAK_REALM")
		var url = "http://" + host + "/auth/realms/" + realmName

		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			log.Fatalln(err)
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		var jaegerTracecontextheadername = "uber-trace-id"
		val, ok := os.LookupEnv("JAEGER_TRACECONTEXTHEADERNAME")
		if ok {
			jaegerTracecontextheadername = val
		}
		if c.Request.Header[jaegerTracecontextheadername] != nil {
			req.Header.Set(jaegerTracecontextheadername, c.Request.Header.Get(jaegerTracecontextheadername))
		}
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			log.Fatalln(err)
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		defer resp.Body.Close()
		bodyBytes, _ := ioutil.ReadAll(resp.Body)

		var realm Realm
		err = json.Unmarshal(bodyBytes, &realm)
		if err != nil {
			log.Fatalln(err)
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		SecretKey := "-----BEGIN CERTIFICATE-----\n" + realm.PublicKey + "\n-----END CERTIFICATE-----"

		mySigningKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(SecretKey))
		if err != nil {
			fmt.Errorf("There was an error parsing pubkey")
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		var plainHeader = authHeader[0]
		re := regexp.MustCompile(`[Bb]earer `)
		var reqToken = re.ReplaceAllString(plainHeader, "")

		token, err := jwt.Parse(reqToken, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
				return nil, fmt.Errorf("There was an error")
			}
			return mySigningKey, nil
		})

		if err != nil {
			fmt.Printf(err.Error())
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": err.Error()})
			return
		}

		if !token.Valid {

			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "Invalid Token"})
			return
		}
		c.Next()
	}
}
